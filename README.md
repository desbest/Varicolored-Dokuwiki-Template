# Varicolored dokuwiki template

* Designed by [Free CSS Templates](https://web.archive.org/web/20130216012302/http://www.freecsstemplates.org/) [also at ajlkn](https://aj.lkn.io/) [also at n33co](http://n33.co)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:varicolored)

![varicolored theme screenshot](https://i.imgur.com/IzvOeGq.png)